#!/bin/bash
set -e
IFS='|'
ENV=$1
CODEGEN="{\
\"generateCode\":false,\
\"generateDocs\":false\
}"

help_output () {
    echo "usage: amplify-publish <--environment|-e <name>>"
    echo "Enable static web hosting for the app on Amazon S3 and deploy the App"
    exit 1
}

add_hosting() {
    echo "# Enabling App hosting in S3 for Env: ${ENV} (amplify add hosting)"
    touch sendInputFromPS.py
    
if [[ ${ENV} = "prod" ]]; then
  cat > sendInputFromPS.py <<EOF
from subprocess import Popen, PIPE
import time

ps = Popen(['amplify','add', 'hosting'], stdin=PIPE)
time.sleep(4)
ps.stdin.write("\x1B[B\n")
time.sleep(1)
ps.stdin.write("\n")
EOF

else    
cat > sendInputFromPS.py <<EOF
from subprocess import Popen, PIPE
import time

ps = Popen(['amplify','add', 'hosting'], stdin=PIPE)
time.sleep(6)
ps.stdin.write("\n")
time.sleep(4)
ps.stdin.write("\n")
time.sleep(4)
ps.stdin.write("\n")
time.sleep(4)
ps.stdin.write("\n")
EOF
  fi
    python sendInputFromPS.py
    echo "# App Status details:"
    amplify env checkout ${ENV}
    amplify status 
}

publish_app () {
    echo "# Publishing new Amplify App in Env: ${ENV} (amplify publish)"
    amplify publish -y --invalidateCloudFront --codegen ${CODEGEN}
    echo "# App Status details:"   
    amplify status 
}

# Check valid environment name
if [[ -z ${ENV} || "${ENV}" =~ [^a-zA-Z0-9\-]+ ]] ; then help_output ; fi
# Add hosting and publish in S3 bucket
  echo "# Environment ${ENV} details:"
  amplify env get --name ${ENV}
  add_hosting
  publish_app 
